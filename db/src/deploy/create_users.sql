-- Deploy ladung:create_users to pg

BEGIN;

  CREATE TABLE users (
    id bigserial NOT NULL,
    user_id bigint,
    username varchar(1024),
    first_name varchar(1024),
    last_name varchar(1024),
    created_at timestamp with time zone NOT NULL,
    last_active_at timestamp with time zone NOT NULL
  );

COMMIT;
