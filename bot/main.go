package main

import (
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/isqad/cazio/bot/usecases"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	app := &App{}
	app.Init()
	defer app.Db.Close()
	defer app.Log.Sync()

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := app.Bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		if txt == "/start" {
			msg := tgbotapi.NewMessage(chId, fmt.Sprintf("%d", getNormDistN()))
			app.Bot.Send(msg)
			continue
		}
	}
}

func getNormDistN() int {
	x := int(rand.NormFloat64()*float64(50.0/3) + float64(50.0))
	if x > 100 {
		x = 100
	}
	if x < 0 {
		x = 0
	}
	return x
}
