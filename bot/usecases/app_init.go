package usecases

import (
	"database/sql"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
	"log"
	"os"
)

type App struct {
	Db  *sql.DB
	Log *zap.Logger
	Bot *tgbotapi.BotAPI
}

func (app *App) Init() {
	var (
		logger *zap.Logger
		err    error
	)

	if os.Getenv(`APP_ENV`) == `production` {
		logger, err = zap.NewProduction()
	} else {
		logger, err = zap.NewDevelopment()
	}

	if err != nil {
		log.Panicf("Can't initialize zap logger: %v", err)
	}

	dbConnSpec := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable",
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_DB"))

	db, err := sql.Open("postgres", dbConnSpec)
	if err != nil {
		logger.Panic("opening db connectoin failed", zap.Error(err))
	}

	err = db.Ping()
	if err != nil {
		logger.Panic("database is not available", zap.Error(err))
	}

	bot, err := tgbotapi.NewBotAPI(os.Getenv("CAZIO_BOT_TOKEN"))
	if err != nil {
		logger.Panic("Can not init bot", zap.Error(err))
	}
	bot.Debug = (os.Getenv(`APP_ENV`) != `production`)

	ctx.Log = logger
	ctx.Db = db
	ctx.Bot = bot
}
